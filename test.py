import sys
from subprocess import Popen, PIPE

TESTS = [
    (['./tp1', 'X', '-', '+', 'NNEE'], 'XXX\nX--\nX--', 0),
    (['./tp1', 'X', '-', '+', 'EEEENNWWSSSSS'], '--XXX\n--X-X\nXX+XX\n--X--\n--X--\n--X--', 0),
    (['./tp1', 'X', '-', '+', 'EEWW'], 'XXX', 0),
    (['./tp1', 'a', 'b', 'c', 'ENSS'], 'ba\nac\nba', 0),
    (['./tp1', 'X', '-', '+', 'EENWWNEE'], 'X+X\n+++\nX+X', 0),
    (['./tp1', 'X', '-', '+', 'EENWWNEE', 'a'], "Erreur: le nombre d'arguments est invalide", 1),
    (['./tp1', 'ab', 'c', 'd', 'EENW'], "Erreur: les cases doivent etre identifiees par des caracteres", 2),
    (['./tp1', 'X', 'X', '+', 'EEN'], 'Erreur: le caractere de case vide doit etre distinct', 3),
    (['./tp1', 'X', '-', '+', 'EENeS'], 'Erreur: les deplacements doivent etre E, N, S ou W', 4),
    (['./tp1', 'X', '-', '+', 'NSNSNSNSNSNSNSNSNSNSNSNSNSNSNSNSNSNSNSNSN'], 'Erreur: la longueur ne doit pas depasser 40', 5),
    (['./tp1', 'X', '-', '+', 'NNNNNNNNNN'], 'Erreur: la hauteur ne doit pas depasser 10', 6),
    (['./tp1', 'X', '-', '+', 'EEEEEEEEEEEEEEE'], 'Erreur: la largeur ne doit pas depasser 15', 7),
]

print '-----------------------'
print 'Testing the program tp1'
print '-----------------------\n'

n = len(TESTS)
i = 1
p = 0
for command, expected_output, expected_code in TESTS:
    print 'Test %s of %s...' % (i, n),
    process = Popen(command, stdout=PIPE, stderr=PIPE)
    output, _ = process.communicate()
    code = process.returncode
    output = output.strip()
    expected_output = expected_output.strip()
    if output == expected_output and code == expected_code:
        print 'passed'
        p += 1
    else:
        print 'failed'
        print 'command:'
        print ' '.join(command)
        print 'output:'
        print output
        print 'expected:'
        print expected_output
        print 'returned code:'
        print code
        print 'expected returned code:'
        print expected_code
    print
    i += 1

print '----------------'
print 'Result: %s / %s' % (p, n)
print '----------------'

if p != n:
    sys.exit('Some tests failed!!!')
