/***************************************************************************
 * Titre:	TRAVAIL PRATIQUE 1
 * Cours:	INF3135 - Construction et maintenance de logiciels
 * Auteure: 	Myrianne Beaudoin-T. 
 * Code:	BEAM26588114
 * Date:	été 2017
 *
 ***************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
 /**---------------------------------------------------------------------------------------------
 * Vérifie si le nombre d'arguments entrés au clavier est de 5
 *
 * @param 	nb 	nombre d'arguments
 * @return 	true 	si nb est égal à 5
 **/
bool validateNbArgs(int nb);
/**--------------------------------------------------------------------------------------------
 * Vérifie si la longueur des arguments, représentant l'état des cases du dessin, est de 1
 *
 * @param	args 	la chaine de caractères entrée au clavier  
 * @return 	true	si la longueur de chaque argument est égal 1
 **/
bool validateNbCar(char *args[]);
/**--------------------------------------------------------------------------------------------
 * Vérifie si chaque argument, représentant l'état des cases du dessin, est distinct.
 *
 * @param	args	la chaîne de caractères entrée au clavier
 * @return	true	si chaque argument est distinct, unique
 **/
bool validateDiffCars(char *args[]);
/**-------------------------------------------------------------------------------------------
 * Vérifie si chaque caractère du chemin est constitué des caractères E, W, E et S
 *
 * @param 	args	la chaîne de caractères entrée au clavier
 * @return	true	si l'argument représentant le chemin n'est constitué que des caractères indiqués
 **/
bool validateDirections(char *args[]);
/**------------------------------------------------------------------------------------------
 * Vérifie si la longueur de l'argument représentant le chemin est, au plus, de taille 40
 *
 * @param 	args	la chaîne de caractères entrée au clavier
 * @return 	true 	si la longueur de l'argument représentant le chemin ne dépasse pas 40
 **/
bool validateLength(char *args[]);
/**-------------------------------------------------------------------------------------------
 * Vérifie si la hauteur du dessin ne dépasse pas 10
 *
 * @param	args	la chaîne de caractères entrée au clavier
 * @return	true	si la hauteur du dessin ne dépasse pas 10
 **/
bool validateHeight(char *args[]);
/**-------------------------------------------------------------------------------------------
 * Vérifie si la largeur du dessin ne dépasse pas 15
 *
 * @param	args	la chaîne de caractères entrée au clavier
 * @return	true 	si la largeur ne dépasse pas 15
 **/
bool validateWidth(char *args[]);
/**------------------------------------------------------------------------------------------
 * Vérifie si la case du dessin, envoyée en paramètre, forme un croisement de chemin
 *
 * @param	grid	la matrice pour enregistrer le chemin du dessin
 * @param	x	la coordonnée x de la case sondée
 * @param	y	la coordonnée y de la case sondée
 * @return	true	si la case a plus de 2 cases voisines occupées
 **/
bool checkNeighbors(char grid[30][20], int x ,int y);
/**-----------------------------------------------------------------------------------------
 * Inscris dans le dessin le caractères envoyé en argument pour les cases vides
 *
 * @param 	args	la chaîne de caractères entrée au clavier
 * @param	grid	la matrice pour enregistrer le chemin du dessin
 **/
void resetGrid(char *args[],char grid[30][20]);
/**-----------------------------------------------------------------------------------------
 * Affiche à l'écran le résultat du dessin tronqué, sans les cases vides superflues,
 * Tout en gardant le format d'un rectangle
 *
 * @param	args	la chaîne de caractères entrée au clavier
 * @param	grid	la matrice pour enregistrer le chemin du dessin
 **/
void draw(char *args[],char grid[30][20]);
/**------------------------------------------------------------------------------------------
 * Donne le maximum entre deux(2) nombres
 *
 * @param	x	nombre 1
 * @param	y	nombre 2
 * @return	max	le nombre le plus grand
 **/
int setMax(int x, int y);
/**----------------------------------------------------------------------------------------
 * Donne le minimum entre deux(2) nombres
 *
 * @param	x 	nombre 1
 * @param	y	nombre 2
 * @return	min	le nombre le plus petit
 **/
int setMin(int x, int y);
/*----------------------------------------------------------------------------------------*/
int main(int argc, char* args[]){
	char grid[30][20];

	if(!validateNbArgs(argc)){ return 1; }
	if(!validateNbCar(args)){ return 2; }
	if(!validateDiffCars(args)){ return 3; }
	if(!validateDirections(args)){ return 4; }
	if(!validateLength(args)) { return 5; }
	if(!validateHeight(args)) { return 6; }
	if(!validateWidth(args)) { return 7; }
	
	resetGrid(args, grid);
	draw(args, grid);	
	
	return 0;
}
bool validateNbArgs(int nb){
	if(nb != 5){
		printf("Erreur: le nombre d'arguments est invalide\n");
		return false;
	}
	return true;
}
bool validateNbCar(char *args[]){
	int i;
		
	for(i=1; i<4; i++){
		int nb = 0, j = 0;
		char* caract = args[i];
	
		while(caract[j] != '\0'){
			nb++;
			j++;
		}
		if(nb>1){
			printf("Erreur: les cases doivent etre identifiees par des caracteres\n");
			return false;
		}		
	}
	return true;
}
bool validateDiffCars(char *args[]){
	char a, b, c;
	a = (char)*args[1];
	b = (char)*args[2];
	c = (char)*args[3];	
	
	if(a == b || b == c || a == c){
		printf("Erreur: le caractere de case vide doit etre distinct\n");
		return false;
	}
	return true;	
}
bool validateDirections(char *args[]){
	int i = 0;
	char *coords = args[4];
	
	while(coords[i] != '\0'){
		char c = coords[i];
		if(c != 'E' &&  c != 'N' && c != 'S' && c != 'W'){
			printf("Erreur: les deplacements doivent etre E, N, S ou W\n");
			return false;
		}
		i++;
	}
	return true;
}

bool validateLength(char *args[]){
	int i = 0, length = 0;
	char *coords = args[4];

	while(coords[i] != '\0'){
		length++;
		i++;		
	}
	if(length > 40) { 
		printf("Erreur: la longueur ne doit pas depasser 40\n");
		return false;
	}	
	return true;
}
bool validateHeight(char *args[]){
	int i = 0, height = 1;
	char *coords = args[4];

	while( coords[i] != '\0'){
		if(coords[i] == 'N'){ height++;}
		if(coords[i] == 'S'){ height--;}
		i++;
	}
	if(height > 10 || height < -10){
		printf("Erreur: la hauteur ne doit pas depasser 10\n");
		return false;
	}
	return true;
}
bool validateWidth(char *args[]){
	int i = 0, width = 1;
	char *coords = args[4];

	while(coords[i] != '\0'){
		if(coords[i] == 'E') { width++;}
		if(coords[i] == 'W') { width--;}
	i++;
	}
	if(width > 15 || width < -15){
		printf("Erreur: la largeur ne doit pas depasser 15\n");
		return false;
	}
	return true;
}
bool checkNeighbors(char grid[30][20], int x, int y){
	int nbOfNeighbors = 0;

	if(grid[x][y] == grid[x-1][y]){ nbOfNeighbors++;}
	if(grid[x][y] == grid[x+1][y]){ nbOfNeighbors++;}
	if(grid[x][y] == grid[x][y-1]){ nbOfNeighbors++;}
	if(grid[x][y] == grid[x][y+1]){ nbOfNeighbors++;}	

	if(nbOfNeighbors < 3){ return false;}
	return true;
}
int setMax(int x, int y){
	if(x<y){ return y;}
	return x;
}
int setMin(int x, int y){
	if(x<y){ return x;}
	return y;
}
void resetGrid(char *args[], char grid[30][20]){
	int x, y;
	
	for(x=0; x<30 ; x++){
		for(y=0; y<20; y++){
			grid[x][y] = (char )*args[2];
		}
	}
}
void  draw(char *args[], char grid[30][20]){
	int x = 15, y = 10, i = 0;
	int minN = 15, maxS = 15, maxE = 10, minW = 10;
	char *coords = args[4];
	
	grid[x][y] = (char)*args[1];
	
	printf("\n");
	while(coords[i] != '\0'){
		char coord = coords[i];
		
		if(coord == 'E'){
			y++;
			maxE = setMax(y, maxE);	
		}else if(coord =='N'){
			x--;
			minN = setMin(x, minN);
		}else if(coord == 'S'){
			x++;
			maxS = setMax(x, maxS);
		}else if(coord == 'W'){
			y--;
			minW = setMin(y, minW);
		}
	
		if(grid[x][y] == (char)*args[1]){
			if(checkNeighbors(grid, x, y)){ 
				grid[x][y] = (char)*args[3];
			}
		}else{
			grid[x][y] = (char)*args[1];
		}
		i++;
	}

	for(x = minN; x < (maxS+1); x++){
		for(y = minW; y < (maxE+1); y++){
			if(grid[x][y] == (char) *args[1] && checkNeighbors(grid, x , y)){
				printf("%c", (char) *args[3]);
			}else{
				printf("%c", grid[x][y]);
			}
		}
		printf("\n");
	}
	printf("\n");	
}
