## TRAVAIL PRATIQUE 1

## Description

- **Cours**: &nbsp;&nbsp;       Construction et maintenance de logiciels
- **Sigle**: &nbsp;&nbsp;       INF3135
- **Université**: &nbsp;&nbsp;  UQAM

Ce programme prend en entrée quatre(4) arguments et dessine, 
en fonction de ces arguments, un chemin dans un rectangle.

## Auteure

- **Prénom**: &nbsp;&nbsp;          Myrianne
- **Nom**: &nbsp;&nbsp;             Beaudoin-Thériault
- **Code permanent**: &nbsp;&nbsp;  BEAM26588114

## Fonctionnement

La commande suivante: `./tp1 <o> <v> <i> <c>`
affiche sur *stdout* une représentation textuelle d'un chemin évoluant dans un rectangle, où:

- `o` est un caractère représentant une case occupée
- `v` est un caractère représentant une case vide
- `i` est un caractère représentant une intersection
- `c` est une chaîne de caractère représentant une suite de déplacements selon les points cardinaux( *E*, *N*, *W* et *S* )

**Attention!**

- Il doit toujours y avoir exactement quatre(4) arguments passés en paramètres
- Les trois(3) arguments représentant l'état des cases doivent être de longueur 1 (un seul caractère)
- Ces trois(3) arguments doivent être distincts, uniques
- Le chemin doit être une suite de caractères choisis parmi *E*, *N*, *S* ou *W*
- La longueur de la chaîne représentant le chemin ne doit pas dépasser 40
- La hauteur du dessin ne doit pas dépasser 10
- La largeur de ce dessin ne doit pas dépasser 15

**Exemple d'utilisation 1**

*Le code* ``	./tp1 X - + EEEENNWWSSSSS	`` *devrait donner*:

```
	--XXX
	--X-X
	XX+XX
	--X--
	--X--
	--X--	 
```

**Exemple d'utilisation 2**

*Le code* ``` ./tp1 X - + EESSWWNNNNWW	``` *devrait donner*:

```
	--XXX
	----X
	XXX-X
	--X-X
	--XXX	
```

## Contenu du projet

- **tp1** :	 	&nbsp;&nbsp;        exécutable
- **README.md** :  	&nbsp;&nbsp;    instructions sur le mode d'emploi du programme pour l'utilisateur
- **Makefile** : 	&nbsp;&nbsp;    instructions pour un nettoyage et une compilation automatique lorsque l'utlisateur entre *make clean* ou *make*
- **.gitignore** : 	&nbsp;&nbsp;    instructions pour éviter le téléversement des fichiers trop lourds et peu pertinents vers **Gitlab**

## Références

Consultation des notes de cours et de **StackOverflow**

## Statut

Projet complet!
